#pragma once

typedef struct EffectColor {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} EffectColor;

typedef void (*EffectOutputCallback) (EffectColor * color, unsigned int index, void * userData);

typedef struct EffectState {
    EffectOutputCallback outputCallback;
    unsigned int         timestamp;
    unsigned int         pointCount;
    void * userData;
} EffectState;

typedef struct SwitchEffectSettings {
    unsigned char lateral;
    unsigned int  duration;
    unsigned char reverse;
    unsigned char mirror;
    unsigned int  colorCount;
    EffectColor * colors;
} SwitchEffectSettings;

typedef struct FadeEffectSettings {
    unsigned char lateral;
    unsigned int  duration;
    unsigned char reverse;
    unsigned char mirror;
    unsigned int  colorCount;
    EffectColor * colors;
} FadeEffectSettings;

typedef struct RainbowEffectSettings {
    unsigned char lateral;
    unsigned int  duration;
    unsigned char reverse;
    unsigned char mirror;
} RainbowEffectSettings;

typedef struct ChaseEffectSettings {
    unsigned char lateral;
    unsigned int  duration;
    unsigned char reverse;
    unsigned char mirror;
    unsigned int  colorCount;
    EffectColor * colors;
} ChaseEffectSettings;

void effectSwitch(EffectState * state, SwitchEffectSettings * settings);
void effectFade(EffectState * state, FadeEffectSettings * settings);
void effectChase(EffectState * state, ChaseEffectSettings * settings);
void effectRainbow(EffectState * state, RainbowEffectSettings * settings);
