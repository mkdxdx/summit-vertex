set(TARGET effects)
add_library(${TARGET} STATIC "")
target_include_directories(${TARGET}
    PUBLIC 
    src
)

target_sources(${TARGET} 
    PRIVATE 
        src/effects.c
)
