add_compile_options(-w -Os)
add_subdirectory(effects)
add_subdirectory(sdk)
add_subdirectory(protocol)