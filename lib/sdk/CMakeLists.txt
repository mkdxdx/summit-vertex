set(TARGET sdk)
add_library(${TARGET} STATIC "")

# modules
set(NRFX_PATH "modules/nrfx")
target_include_directories(${TARGET}
    PUBLIC
        ${NRFX_PATH}
        ${NRFX_PATH}/drivers
        ${NRFX_PATH}/drivers/include
        ${NRFX_PATH}/drivers/src
        ${NRFX_PATH}/drivers/src/prs
        ${NRFX_PATH}/hal
        ${NRFX_PATH}/mdk
        ${NRFX_PATH}/soc
        ${NRFX_PATH}/helpers/nrfx_gppi
)
target_sources(${TARGET}
    PRIVATE
        ${NRFX_PATH}/drivers/src/nrfx_adc.c
        ${NRFX_PATH}/drivers/src/nrfx_clock.c
        ${NRFX_PATH}/drivers/src/nrfx_comp.c
        ${NRFX_PATH}/drivers/src/nrfx_dppi.c
        ${NRFX_PATH}/drivers/src/nrfx_gpiote.c
        ${NRFX_PATH}/drivers/src/nrfx_i2s.c
        ${NRFX_PATH}/drivers/src/nrfx_lpcomp.c
        ${NRFX_PATH}/drivers/src/nrfx_nfct.c
        ${NRFX_PATH}/drivers/src/nrfx_nvmc.c
        ${NRFX_PATH}/drivers/src/nrfx_pdm.c
        ${NRFX_PATH}/drivers/src/nrfx_power.c
        ${NRFX_PATH}/drivers/src/nrfx_ppi.c
        ${NRFX_PATH}/drivers/src/nrfx_pwm.c
        ${NRFX_PATH}/drivers/src/nrfx_qdec.c
        ${NRFX_PATH}/drivers/src/nrfx_qspi.c
        ${NRFX_PATH}/drivers/src/nrfx_rng.c
        ${NRFX_PATH}/drivers/src/nrfx_rtc.c
        ${NRFX_PATH}/drivers/src/nrfx_saadc.c
        ${NRFX_PATH}/drivers/src/nrfx_spi.c
        ${NRFX_PATH}/drivers/src/nrfx_spim.c
        ${NRFX_PATH}/drivers/src/nrfx_spis.c
        ${NRFX_PATH}/drivers/src/nrfx_swi.c
        ${NRFX_PATH}/drivers/src/nrfx_systick.c
        ${NRFX_PATH}/drivers/src/nrfx_temp.c
        ${NRFX_PATH}/drivers/src/nrfx_timer.c
        ${NRFX_PATH}/drivers/src/nrfx_twi.c
        ${NRFX_PATH}/drivers/src/nrfx_twi_twim.c
        ${NRFX_PATH}/drivers/src/nrfx_twim.c
        ${NRFX_PATH}/drivers/src/nrfx_twis.c
        ${NRFX_PATH}/drivers/src/nrfx_uart.c
        ${NRFX_PATH}/drivers/src/nrfx_uarte.c
        ${NRFX_PATH}/drivers/src/nrfx_usbd.c
        ${NRFX_PATH}/drivers/src/nrfx_wdt.c
        ${NRFX_PATH}/drivers/src/prs/nrfx_prs.c
        ${NRFX_PATH}/hal/nrf_ecb.c
        ${NRFX_PATH}/hal/nrf_nvmc.c
        ${NRFX_PATH}/soc/nrfx_atomic.c
)

# integration and legacy
set(NRFX_INT_PATH "integration/nrfx")
target_include_directories(${TARGET}
    PUBLIC
        ${NRFX_INT_PATH}
)

# components
set(LIBRARIES_PATH "components/libraries")
target_include_directories(${TARGET}
    PUBLIC
        components
        ${LIBRARIES_PATH}/atomic
        ${LIBRARIES_PATH}/balloc
        ${LIBRARIES_PATH}/libuarte
        ${LIBRARIES_PATH}/experimental_section_vars
        ${LIBRARIES_PATH}/delay
        ${LIBRARIES_PATH}/fifo
        ${LIBRARIES_PATH}/gpiote
        ${LIBRARIES_PATH}/hardfault
        ${LIBRARIES_PATH}/log
        ${LIBRARIES_PATH}/log/src
        ${LIBRARIES_PATH}/queue
        ${LIBRARIES_PATH}/ringbuf
        ${LIBRARIES_PATH}/simple_timer
        ${LIBRARIES_PATH}/strerror
        ${LIBRARIES_PATH}/svc
        ${LIBRARIES_PATH}/timer
        ${LIBRARIES_PATH}/uart
        ${LIBRARIES_PATH}/util
        components/drivers_nrf/nrf_soc_nosd
        components/toolchain/cmsis/include
        components/toolchain/arm
        components/proprietary_rf/esb
)
target_sources(${TARGET}
    PRIVATE
        ${LIBRARIES_PATH}/atomic/nrf_atomic.c
        ${LIBRARIES_PATH}/balloc/nrf_balloc.c
        ${LIBRARIES_PATH}/experimental_section_vars/nrf_section_iter.c
        ${LIBRARIES_PATH}/fifo/app_fifo.c
        ${LIBRARIES_PATH}/gpiote/app_gpiote.c
        ${LIBRARIES_PATH}/hardfault/hardfault_implementation.c
        ${LIBRARIES_PATH}/hardfault/nrf52/handler/hardfault_handler_gcc.c
        ${LIBRARIES_PATH}/log/src/nrf_log_default_backends.c
        ${LIBRARIES_PATH}/log/src/nrf_log_frontend.c
        ${LIBRARIES_PATH}/log/src/nrf_log_str_formatter.c
        ${LIBRARIES_PATH}/queue/nrf_queue.c
        ${LIBRARIES_PATH}/ringbuf/nrf_ringbuf.c
        ${LIBRARIES_PATH}/simple_timer/app_simple_timer.c
        ${LIBRARIES_PATH}/strerror/nrf_strerror.c
        ${LIBRARIES_PATH}/svc/nrf_svc_handler.c
        ${LIBRARIES_PATH}/timer/app_timer.c
        ${LIBRARIES_PATH}/uart/app_uart.c
        ${LIBRARIES_PATH}/uart/app_uart_fifo.c
        ${LIBRARIES_PATH}/util/app_error.c
        ${LIBRARIES_PATH}/util/app_error_handler_gcc.c
        ${LIBRARIES_PATH}/util/app_error_weak.c
        ${LIBRARIES_PATH}/util/app_util_platform.c
        ${LIBRARIES_PATH}/util/nrf_assert.c
        ${LIBRARIES_PATH}/util/sdk_mapped_flags.c
        ${LIBRARIES_PATH}/libuarte/nrf_libuarte_drv.c
        ${LIBRARIES_PATH}/libuarte/nrf_libuarte_async.c
        components/drivers_nrf/nrf_soc_nosd/nrf_nvic.c
        components/drivers_nrf/nrf_soc_nosd/nrf_soc.c
        components/proprietary_rf/esb/nrf_esb.c
)

target_include_directories(${TARGET}
    PUBLIC
        config/nrf52840/config
)

target_sources(${TARGET}
    PRIVATE
        ${NRFX_PATH}/mdk/system_nrf52840.c
        ${NRFX_PATH}/mdk/gcc_startup_nrf52840.S
)