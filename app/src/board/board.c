#include "nrf_gpio.h"
#include "nrfx_spim.h"
#include "nrfx_qdec.h"
#include "nrfx_systick.h"
#include "nrfx_pwm.h"
#include "nrf.h"
#include "board.h"
#include "serialLed.h"
#include <string.h>
#include <stdint.h>

// button pin def
#define PIN_BUTTON     NRF_GPIO_PIN_MAP(1, 13)

// external rgb led pins for pwm - R,G,B
#define PWM_0_PIN_0   NRF_GPIO_PIN_MAP(1, 11)
#define PWM_0_PIN_1   NRF_GPIO_PIN_MAP(1, 12)
#define PWM_0_PIN_2   NRF_GPIO_PIN_MAP(1, 15)

// onboard rgb led pins for pwm - R,G,B
#define PWM_1_PIN_0   NRF_GPIO_PIN_MAP(0, 24)
#define PWM_1_PIN_1   NRF_GPIO_PIN_MAP(0, 16)
#define PWM_1_PIN_2   NRF_GPIO_PIN_MAP(0, 6)

typedef struct BoardGpio {
    uint32_t gpio;
} BoardGpio;

static const BoardGpio BoardPins[BOARD_PIN_COUNT] = {
    [BOARD_PIN_BUTTON]       = { .gpio = PIN_BUTTON },
};
static const nrfx_pwm_t BoardPwm = NRFX_PWM_INSTANCE(0);
static uint16_t pwmData[4] = {0, 0, 0, 0};

static volatile uint32_t tick = 0;

static void gpioInit(void)
{
    nrf_gpio_cfg_input(PIN_BUTTON, NRF_GPIO_PIN_PULLUP);
}

static inline void waitRdy(void)
{
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}
}

static void pwmInit(void)
{

}

void boardInit(void) {
    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_HFCLKSTART = 1;
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);

    nrfx_systick_init();

    gpioInit();
}

void boardSetPin(BoardPin pin, bool state)
{
    if (pin >= BOARD_PIN_COUNT)
        return;

    if (state) {
        nrf_gpio_pin_set(BoardPins[pin].gpio);
    } else {
        nrf_gpio_pin_clear(BoardPins[pin].gpio);
    }

}

bool boardGetPin(BoardPin pin)
{
    if (pin >= BOARD_PIN_COUNT)
        return false;

    return nrf_gpio_pin_read(BoardPins[pin].gpio) > 0;
}

void boardGetChipUid(uint8_t data[BOARD_CHIP_ID_LEN])
{
    volatile uint32_t *idAddr = NRF_FICR->DEVICEID;
    memcpy(data, idAddr, BOARD_CHIP_ID_LEN);
}

void boardFlashPageWrite(uint32_t address, uint8_t data[BOARD_SECTOR_SIZE])
{
    // erase enable
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Een;
    __ISB();
    __DSB();

    // erase
    NRF_NVMC->ERASEPAGE = address;
    waitRdy();

    // write enable
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;
    __ISB();
    __DSB();

    // write
    uint32_t *src = (uint32_t *)data;
    for (uint32_t i = 0; i < BOARD_SECTOR_SIZE / sizeof(uint32_t); i++) {
        ((uint32_t*)address)[i] = src[i];
        waitRdy();
    }

    // write disable
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
    __ISB();
    __DSB();
}

void boardDelayMs(uint32_t ms)
{
    nrfx_systick_delay_ms(ms);
}

void boardEnablePwm(bool enable, BoardPwmOutType type)
{
    if (type >= BOARD_PWM_OUT_TYPE_COUNT) {
        return;
    }

    static const struct {
        uint8_t pin[NRF_PWM_CHANNEL_COUNT]
    } PwmPins[BOARD_PWM_OUT_TYPE_COUNT] = {
        [BOARD_PWM_OUT_TYPE_1] = {
            PWM_0_PIN_0, PWM_0_PIN_1, PWM_0_PIN_2, NRF_PWM_PIN_NOT_CONNECTED
        },
        [BOARD_PWM_OUT_TYPE_2] = {
            PWM_1_PIN_0, PWM_1_PIN_1, PWM_1_PIN_2, NRF_PWM_PIN_NOT_CONNECTED
        },
    };
    static bool active = false;

    uint32_t ret;
    if (enable) {
        nrfx_pwm_config_t cfg = {
            .base_clock = NRF_PWM_CLK_4MHz,
            .count_mode = NRF_PWM_MODE_UP,
            .top_value = 0xFF,
            .load_mode = NRF_PWM_LOAD_INDIVIDUAL,
            .step_mode = NRF_PWM_STEP_AUTO
        };
        memcpy(cfg.output_pins, PwmPins[type].pin, sizeof(cfg.output_pins));
        nrf_pwm_sequence_t seq = {
            .values = {
                .p_raw = pwmData
            },
            .length = sizeof(pwmData) / sizeof(*pwmData),
            .repeats = 1,
        };

        if (active) {
            boardEnablePwm(false, type);
        }
        memset(pwmData, 0, sizeof(pwmData));
        ret = nrfx_pwm_init(&BoardPwm, &cfg, NULL);
        APP_ERROR_CHECK(ret);
        ret = nrfx_pwm_simple_playback(&BoardPwm, &seq, 1, NRFX_PWM_FLAG_LOOP);
        APP_ERROR_CHECK(ret);
    } else {
        if (active) {
            nrfx_pwm_uninit(&BoardPwm);
        }
        active = false;
    }
}

void boardSetPwmChannelValue(uint8_t channel, uint8_t value)
{
    if (channel > sizeof(pwmData) / sizeof(*pwmData)) {
        return;
    }
    pwmData[channel] = value;
}