#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#define BOARD_CHIP_ID_LEN (16U)
#define BOARD_SECTOR_SIZE (4096U)
#define BOARD_PWM_CHANNEL_COUNT (3U)

typedef enum {
    BOARD_PIN_BUTTON,
    BOARD_PIN_COUNT
} BoardPin;

// pwm type is solely to distinguish
// two different pwm output (pin groups)
typedef enum {
    BOARD_PWM_OUT_TYPE_1,
    BOARD_PWM_OUT_TYPE_2,
    BOARD_PWM_OUT_TYPE_COUNT
} BoardPwmOutType;

void boardInit(void);
void boardGetChipUid(uint8_t data[BOARD_CHIP_ID_LEN]);
void boardSetPin(BoardPin pin, bool state);
bool boardGetPin(BoardPin pin);
void boardFlashPageWrite(uint32_t address, uint8_t data[BOARD_SECTOR_SIZE]);
void boardDelayMs(uint32_t ms);
// enables pwm peripheral and pwm pins according to board layer definitions
// only one pwm output type can be active
// activating output of different type deactivates other type
void boardEnablePwm(bool enable, BoardPwmOutType type);
void boardSetPwmChannelValue(uint8_t channel, uint8_t value);