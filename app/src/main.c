#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "radio.h"
#include "serialLed.h"
#include "board.h"
#include "protocol.h"
#include "effects.h"
#include "pairing.h"

#define MAX_BUFFER_SIZE       300
#define MAX_PALETTE_COLORS    10
#define DEFAULT_RADIO_CHANNEL 15

#pragma pack(push,1)
typedef struct StaticEffectSettings {
    EffectId effect;
    bool lateral;
    bool mirror;
    bool reverse;
    uint32_t periodMs;
    uint32_t paletteColorOffset;
    uint32_t paletteColorCount;
    EffectColor palette[MAX_PALETTE_COLORS];
} StaticEffectSettings;

typedef struct SystemSettings {
    uint32_t channelCount;
    uint32_t driverType;
    uint32_t  commChannel;
    uint8_t  keyCanBlank;
} SystemSettings;

typedef struct Settings {
    SystemSettings       system;
    StaticEffectSettings staticEffect;
} Settings;
#pragma pack(pop)

static void processDisplay(void);
static void processConfig(ConfigPairListHeader * config);
static void processLedData(ChannelDataHeader * channelData);
static void processEffect(EffectDataHeader *effect);
static bool isButtonPressed(void);
static void effectOutputCallback(EffectColor * color, unsigned int index, void * userData);
static void loadSettings(Settings *settingsData);
static void saveSettings(const Settings *settingsData);
static void resetSettings(void);

static const EffectColor BlackColor = {0, 0, 0};
static const uint8_t AckPld[] = {0xAA};

static const SerialLedColor BlackColorSerial[SERIAL_LED_COUNT] = {
    {0, 0, 0}
};

// output operation
static union {
    uint8_t pwm[3];
    SerialLedColor serial[SERIAL_LED_COUNT];
} ledData;
static bool          updateOutput    = true;
static bool          useStaticEffect = true;
static bool          settingsUpdated = false;
static bool buttonState = false;

// packet processing
static uint8_t MyAddress[ADDRESS_LENGTH] = {};
static bool txOk = false;
static bool txBusy = false;

// settings
static uint8_t radioChannel = 0;
static Settings settings = {{0}, {0}};
static uint8_t settingsPage[BOARD_SECTOR_SIZE] 
        __attribute__((section(".settings_page"))) __attribute__((used));

static void decodeCommand(const uint8_t *data, size_t size)
{
    DataHeader *header = (DataHeader *)data;
    uint8_t *payload = ((uint8_t *)header) + sizeof(DataHeader);
    switch (header->dataId) {
    case DataIdChannelData:
        processLedData((ChannelDataHeader *)(payload));
        break;
    case DataIdConfigPairList:
        processConfig((ConfigPairListHeader *)(payload));
        break;
    case DataIdEffect:
        processEffect((EffectDataHeader *)(payload));
        break;
    default:
        break;
    }
}

static void decodePacket(const uint8_t *data, size_t size)
{
#define START_CHAR (0x80U)
#define END_CHAR (0x40U)
#define SEQ_MASK (0x3FU)

    if (data == NULL || size == 0) {
        return;
    }

    static uint8_t receiveBuffer[1024];
    static uint32_t receiveOffset = 0;

    uint8_t isStart = (data[0] & START_CHAR) != 0;
    uint8_t isEnd = (data[0] & END_CHAR) != 0;
    uint8_t seq = (data[0] & SEQ_MASK);

    if (isStart && isEnd && seq != 0) {
        // sequence error
        receiveOffset = 0;
        return;
    }

    if (isStart) {
        receiveOffset = 0;
    }

    if (receiveOffset + size > sizeof(receiveBuffer)) {
        // size error
        receiveOffset = 0;
        return;
    }

    memcpy(&receiveBuffer[receiveOffset], &data[1], size - 1);
    receiveOffset += size - 1;
    if (isEnd) {
        // process buffer
        decodeCommand(receiveBuffer, receiveOffset);
        receiveOffset = 0;
    }
#undef START_CHAR
#undef END_CHAR
#undef SEQ_MASK
}

static void rxCb(const uint8_t data[RADIO_DATA_MAX_LENGTH], 
                 size_t size)
{
    decodePacket(data, size);
    radioWriteAckPayload(AckPld, sizeof(AckPld));
}

static void txCb(bool success)
{
    txOk = success;
    txBusy = false;
}

int main(void)
{
    boardInit();
    memset(&ledData, 0, sizeof(ledData));
    if (isButtonPressed()) {
        resetSettings();
    }
    loadSettings(&settings);
    uint8_t uidTemp[BOARD_CHIP_ID_LEN];
    boardGetChipUid(uidTemp);
    memcpy(MyAddress, uidTemp, sizeof(MyAddress));
    memset(&ledData, 0, sizeof(ledData));

    if (isButtonPressed()) {
        pairingProcess();
    }
    radioInit(rxCb, txCb, true, MyAddress, radioChannel);
    radioWriteAckPayload(AckPld, sizeof(AckPld));
    while (1) {
        processDisplay();
    }
}

static void processOutput(void)
{
    bool writeBlack = false;

    if (settings.system.keyCanBlank != 0 && !buttonState) {
        writeBlack = true;
    }

    if (settings.system.driverType == ConfigDriverTypePixel1) {
        if (writeBlack) {
            serialLedSend(BlackColorSerial, SERIAL_LED_COUNT);
        } else {
            serialLedSend(ledData.serial, SERIAL_LED_COUNT);
        }
    }
    if (settings.system.driverType == ConfigDriverTypePwm
        || settings.system.driverType == ConfigDriverTypePwmInternal) {
        if (writeBlack) {
            boardSetPwmChannelValue(0, 0);
            boardSetPwmChannelValue(1, 0);
            boardSetPwmChannelValue(2, 0);
        } else {
            boardSetPwmChannelValue(0, ledData.pwm[0]);
            boardSetPwmChannelValue(1, ledData.pwm[1]);
            boardSetPwmChannelValue(2, ledData.pwm[2]);
        }
        
    }
}

static void processDisplay(void)
{
    static uint32_t elapsedMs = 0;

    if (settingsUpdated) {
        settingsUpdated = false;
        saveSettings(&settings);
        loadSettings(&settings);
    }

    if (useStaticEffect) {
        static uint32_t lastEffectTs = 0;
        if (elapsedMs - lastEffectTs >= 40) {
            lastEffectTs = elapsedMs;
            StaticEffectSettings effectSettings = settings.staticEffect;
            uint32_t pointCnt = settings.system.channelCount / 3;
            uint32_t colOffset = effectSettings.paletteColorOffset < effectSettings.paletteColorCount
                                    ? effectSettings.paletteColorOffset
                                    : effectSettings.paletteColorCount - 1;
            if (effectSettings.effect == EffectIdFill) {
                for (uint32_t i = 0; i < pointCnt; i++) {
                    effectOutputCallback(&effectSettings.palette[colOffset], i, NULL);
                }
            } else {
                EffectState eState;
                eState.timestamp = lastEffectTs;
                eState.pointCount = pointCnt;
                eState.outputCallback = effectOutputCallback;
                RainbowEffectSettings rainbow;
                ChaseEffectSettings chase;
                FadeEffectSettings fade;
                SwitchEffectSettings switchEff;
                switch (effectSettings.effect) {
                case EffectIdSwitch:
                    switchEff.colors = effectSettings.palette;
                    switchEff.colorCount = effectSettings.paletteColorCount;
                    switchEff.duration = effectSettings.periodMs;
                    switchEff.lateral = effectSettings.lateral;
                    switchEff.mirror = effectSettings.mirror;
                    switchEff.reverse = effectSettings.reverse;
                    effectSwitch(&eState, &switchEff);
                    break;
                case EffectIdFade:
                    fade.colors = effectSettings.palette;
                    fade.colorCount = effectSettings.paletteColorCount;
                    fade.duration = effectSettings.periodMs;
                    fade.lateral = effectSettings.lateral;
                    fade.mirror = effectSettings.mirror;
                    fade.reverse = effectSettings.reverse;
                    effectFade(&eState, &fade);
                    break;
                case EffectIdChase:
                    chase.colors = effectSettings.palette;
                    chase.colorCount = effectSettings.paletteColorCount;
                    chase.duration = effectSettings.periodMs;
                    chase.lateral = effectSettings.lateral;
                    chase.mirror = effectSettings.mirror;
                    chase.reverse = effectSettings.reverse;
                    effectChase(&eState, &chase);
                    break;
                case EffectIdRainbow:
                    rainbow.duration = effectSettings.periodMs;
                    rainbow.lateral = effectSettings.lateral;
                    rainbow.mirror = effectSettings.mirror;
                    rainbow.reverse = effectSettings.reverse;
                    effectRainbow(&eState, &rainbow);
                    break;
                default:
                    for (uint32_t i = 0; i < pointCnt; i++) {
                        effectOutputCallback(&BlackColor, i, NULL);
                    }
                    break;
                }
            }
            updateOutput = true;
        }
    }

    if (buttonState != isButtonPressed() && settings.system.keyCanBlank != 0) {
        updateOutput = settings.system.keyCanBlank != 0;
        buttonState = isButtonPressed();
    }

    if (updateOutput) {
        processOutput();
    }    
    elapsedMs++;
}

static bool isButtonPressed(void)
{
    return !boardGetPin(BOARD_PIN_BUTTON);
}

static void processConfig(ConfigPairListHeader * config)
{
    ConfigPair * cfgPair = (ConfigPair *)((uint8_t *)config + sizeof(ConfigPairListHeader));
    for (uint32_t i = 0; i < config->configPairCount; i++) {
        switch(cfgPair[i].configKey) {
            case ConfigKeyChannelCount:
                settings.system.channelCount = cfgPair[i].configValue <= sizeof(ledData) ? cfgPair[i].configValue : sizeof(ledData) ;
                settingsUpdated |= true;
                break;
            case ConfigKeyGateOverride:
                settings.system.keyCanBlank = cfgPair[i].configValue == 1 ? true : false;
                settingsUpdated |= true;
                buttonState = false;
                break;
            case ConfigKeyCommChannel:
                settings.system.commChannel = cfgPair[i].configValue;
                radioChannel = cfgPair[i].configValue;
                settingsUpdated |= true;
                break;
            case ConfigKeyDriverType:
                settings.system.driverType = cfgPair[i].configValue;
                settingsUpdated |= true;
                break;
            default:
                break;
        }
    }
}

static void processLedData(ChannelDataHeader * channelData)
{
    if (channelData->dataOffset + channelData->dataSize > sizeof(ledData) || channelData->dataSize == 0)
        return;
    memset(&ledData, 0, sizeof(ledData));
    memcpy(&ledData.serial[channelData->dataOffset],
           ((uint8_t *)channelData + sizeof(ChannelDataHeader)),
           channelData->dataSize);
    useStaticEffect = false;
    if (settings.system.keyCanBlank == 0) {
        updateOutput = true;
    }
}


static void processEffect(EffectDataHeader *effect)
{
    (void)effect;

    StaticEffectSettings effectSettings = {0};
    effectSettings.effect = effect->effectId;
    effectSettings.lateral = effect->lateral > 0;
    effectSettings.mirror = effect->mirrored > 0;
    effectSettings.reverse = effect->reverse > 0;
    effectSettings.periodMs = effect->effectPeriod;
    effectSettings.paletteColorOffset = effect->paletteColorOffset;
    effectSettings.paletteColorCount = effect->paletteColorCount <= MAX_PALETTE_COLORS
                                        ? effect->paletteColorCount
                                        : MAX_PALETTE_COLORS;
    EffectColor *effPalette = (EffectColor *)&effect[1];
    for (uint32_t i = 0; i < effectSettings.paletteColorCount; i++) {
        effectSettings.palette[i] = effPalette[i];
    }
    settings.staticEffect = effectSettings;
    settingsUpdated = true;
    useStaticEffect = true;
}

static void effectOutputCallback(EffectColor * color, unsigned int index, void * userData)
{
    (void)userData;
    if (settings.system.driverType == ConfigDriverTypePwm
        || settings.system.driverType == ConfigDriverTypePwmInternal) {
        if (index > 0) {
            return;
        }

        ledData.pwm[0] = color->r;
        ledData.pwm[1] = color->g;
        ledData.pwm[2] = color->b;
    } else if (settings.system.driverType == ConfigDriverTypePixel1) {
        if (index >= SERIAL_LED_COUNT) {
            return;
        }

        ledData.serial[index].r = color->r;
        ledData.serial[index].g = color->g;
        ledData.serial[index].b = color->b;
    }    
}

static void loadSettings(Settings *settingsData)
{
    memcpy(settingsData, settingsPage, sizeof(*settingsData));
    radioChannel = settingsData->system.commChannel;
    switch (settings.system.driverType) {
    case ConfigDriverTypePixel1:
        boardEnablePwm(false, 0);
        serialLedInit();
        break;
    case ConfigDriverTypePwm:
        serialLedUninit();
        boardEnablePwm(true, BOARD_PWM_OUT_TYPE_1);
        break;
    case ConfigDriverTypePwmInternal:
        serialLedUninit();
        boardEnablePwm(true, BOARD_PWM_OUT_TYPE_2);
        break;
    }
}

static void saveSettings(const Settings *settingsData)
{
    uint8_t _Alignas(uint32_t) buffer[BOARD_SECTOR_SIZE];
    memcpy(buffer, settingsData, sizeof(*settingsData));
    boardFlashPageWrite((uint32_t)settingsPage, buffer);
}

static void resetSettings(void)
{
    static const Settings DefaultSettings = {
        .system = {
            .channelCount = 3,
            .keyCanBlank  = 1,
            .driverType   = ConfigDriverTypePixel1,
            .commChannel  = DEFAULT_RADIO_CHANNEL
        },
        .staticEffect =  {
            .effect             = EffectIdRainbow,
            .lateral            = 0,
            .mirror             = 0,
            .reverse            = 0,
            .periodMs           = 4000,
            .paletteColorOffset = 0,
            .paletteColorCount  = 1,
            .palette = {
                {.r = 255, .g = 0, .b = 0}
            }
        }
    };
    saveSettings(&DefaultSettings);
}