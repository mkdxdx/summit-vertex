#include <stdint.h>
#include <string.h>
#include "serialLed.h"
#include "nrf_delay.h"
#include "nrfx_pwm.h"
#include "nrf_gpio.h"

#define PWM_FREQ (16000000UL)
#define LED_DATA_FREQ (800000UL)

#define LED_BITS_PER_COLOR (8U)
#define LED_COLORS_PER_LED (3U)
#define LED_SEQUENCE_SIZE (SERIAL_LED_COUNT \
                            * LED_COLORS_PER_LED \
                            * LED_BITS_PER_COLOR \
                            + 1U)
#define LED_PWM_0 (0x8000U | (6U))
#define LED_PWM_1 (0x8000U | (12U))
#define LED_PIN NRF_GPIO_PIN_MAP(1, 11)

static const nrfx_pwm_t LedPwm = NRFX_PWM_INSTANCE(0);

static uint16_t ledSequence[LED_SEQUENCE_SIZE] = {0};
static bool finished = false;
static bool active = false;

static void pwmEvtHandler(nrfx_pwm_evt_type_t evt)
{
    if (evt == NRFX_PWM_EVT_FINISHED) {
        finished = true;
    }
}

void serialLedInit(void)
{
    const nrfx_pwm_config_t cfg = {
        .output_pins = {
            LED_PIN, 
            NRFX_PWM_PIN_NOT_USED, 
            NRFX_PWM_PIN_NOT_USED, 
            NRFX_PWM_PIN_NOT_USED
        },
        .irq_priority = 4,
        .base_clock = NRF_PWM_CLK_16MHz,
        .count_mode = NRF_PWM_MODE_UP, 
        .top_value = PWM_FREQ / LED_DATA_FREQ,
        .load_mode = NRF_PWM_LOAD_COMMON,
        .step_mode = NRF_PWM_STEP_AUTO,
    };

    /*
    nrf_gpio_cfg(LedPin,
                 NRF_GPIO_PIN_DIR_OUTPUT,
                 NRF_GPIO_PIN_INPUT_DISCONNECT,
                 NRF_GPIO_PIN_NOPULL,
                 NRF_GPIO_PIN_S0D1,
                 NRF_GPIO_PIN_NOSENSE);
    */

    if (!active) {
        uint32_t ret = nrfx_pwm_init(&LedPwm, &cfg, pwmEvtHandler);
        APP_ERROR_CHECK(ret);
    }
    active = true;
}

void serialLedUninit(void)
{
    if (active) {
        nrfx_pwm_uninit(&LedPwm);
    }
    active = false;
}

void serialLedSend(const SerialLedColor *data, size_t count)
{
    if (count > SERIAL_LED_COUNT * LED_COLORS_PER_LED || !active) {
        return;
    }

    memset(ledSequence, 0, sizeof(ledSequence));

    uint16_t *ptr = ledSequence;
    for (uint32_t led = 0; led < count; led++) {
        for (uint32_t byte = 0; byte < LED_COLORS_PER_LED; byte++) {
            uint8_t byteData = data[led].byte[byte];
            for (uint32_t bit = 0; bit < LED_BITS_PER_COLOR; bit++) {
                if ((byteData & (0x80 >> bit)) != 0) {
                    *ptr = LED_PWM_1;
                } else {
                    *ptr = LED_PWM_0;
                }
                ptr++;
            }
        }
    }
    ledSequence[LED_SEQUENCE_SIZE - 1] = (0 << 15) | 25;

    nrf_pwm_sequence_t seq = {
        .values = {
            .p_raw = ledSequence
        },
        .length = LED_SEQUENCE_SIZE,
        .repeats = 0,
        .end_delay = 0,
    };

    finished = false;
    uint32_t ret = nrfx_pwm_simple_playback(&LedPwm, &seq, 1, 0);
    APP_ERROR_CHECK(ret);
    while (!finished) {}
}