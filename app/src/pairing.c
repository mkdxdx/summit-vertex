
#include "pairing.h"
#include "radio.h"
#include "board.h"
#include "nrf_gpio.h"
#include <string.h>

// change this on other board
#define LED_PIN  NRF_GPIO_PIN_MAP(0, 13)
#define DEFAULT_RADIO_CHANNEL 15
#define PAIRING_HEADER  (0xFFU)

static uint8_t ackData[7] = {
    PAIRING_HEADER, ADDRESS_LENGTH
};

/* Receiver address */

static uint8_t receivedData[255];
static uint32_t receivedDataLen = 0;

static void rxCb(const uint8_t data[RADIO_DATA_MAX_LENGTH], 
                 size_t size)
{
    receivedDataLen = size;
    memcpy(receivedData, data, size);
    radioWriteAckPayload(ackData, sizeof(ackData));
}

static void txCb(bool success)
{
    (void)success;
}

static bool isButtonPressed(void)
{
    return !boardGetPin(BOARD_PIN_BUTTON);
}

void pairingProcess(void)
{
    // set radio as slave
    // set pairing address
    // write ack with receive address
    // wait for messages while button is pressed
    nrf_gpio_cfg_output(LED_PIN);
    nrf_gpio_pin_set(LED_PIN);
    uint8_t uidTemp[BOARD_CHIP_ID_LEN];
    boardGetChipUid(uidTemp);
    memcpy(&ackData[2], uidTemp, ADDRESS_LENGTH);
    receivedDataLen = 0;
    receivedData[0] = 0;
    static const uint8_t UnboundRxAddress[5] = { 0xE7, 0xE7, 0xE7, 0xE7, 0xE7 };
    radioInit(rxCb, txCb, true, UnboundRxAddress, DEFAULT_RADIO_CHANNEL);
    radioWriteAckPayload(ackData, sizeof(ackData));

    while(isButtonPressed()) {
        if (receivedDataLen > 0) {
            if (receivedData[0] == PAIRING_HEADER && receivedData[1] == ADDRESS_LENGTH) {
                // received data succesfuly, exit presence/pairing
                break;
            }
            receivedDataLen = 0;
        }
    }
    boardDelayMs(10);
    radioDisable();
    nrf_gpio_pin_clear(LED_PIN);
    nrf_gpio_cfg_default(LED_PIN);
}