#pragma once

#include <stdint.h>
#include <stddef.h>

#define SERIAL_LED_COUNT (128U)

#pragma pack(push,1)
typedef union {
    struct {
        uint8_t g;
        uint8_t r;
        uint8_t b;
    };
    uint8_t byte[3];
} SerialLedColor;
#pragma pack(pop)

void serialLedInit(void);
void serialLedUninit(void);
void serialLedSend(const SerialLedColor *data, size_t count);
